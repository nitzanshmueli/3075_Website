<%@ page language="java" contentType="text/html; charset=windows-1255" pageEncoding="windows-1255"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>Checker</title>
</head>
<body>
<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
     url="jdbc:mysql://localhost/userdb"
     user="root"  password="Royboy007"/>
     
<sql:query dataSource="${snapshot}" var="result">
select * from members where user ='${param.uname}';
</sql:query>

   


<c:choose>
    <c:when test="${param.pass==result.rows[0].pass}">
        <%session.setAttribute("user", request.getParameter("uname")); %>
        <c:set var="isAdmin" value = "${result.rows[0].admin}" scope = "session"/>
        <c:set var="loggedIn" value = "1" scope = "session"/>
        <%session.setMaxInactiveInterval(-1); %>
        <%response.sendRedirect("userInfo.jsp"); %>
       
    </c:when>
    <c:otherwise>
    <script>window.alert("Passwords do not match");</script>
    
    <a href = "index.jsp">Return to login page</a>
    </c:otherwise>
</c:choose>
 
</body>
</html>