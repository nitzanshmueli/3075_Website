<%@ page language="java" contentType="text/html; charset=windows-1255" pageEncoding="windows-1255"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Signup</title>
<link rel="stylesheet" type="text/css" href="Style.css">
</head>
<body>
<img src="${pageContext.request.contextPath}/images/pen.jpg" height = "500px" width = "1000px" align = "middle"/>
<a name="home"></a>
<div>
<h1>Welcome to 3075's Website!!!!!</h1>
<form id="signup" action="DatabaseUsage.jsp" method="get">
<ul>
  <li><a href="index.jsp">Home</a></li>
  <c:if test = "${loggedIn == '1'}">
  	<li><a href = "userInfo.jsp">User Info</a></li>
  </c:if>
    <li><a href="about.jsp">About</a></li>
  <li><a href="ContactUs.jsp">Contact</a></li>
  <c:if test = "${loggedIn == NULL}">
  	<li><a href="Signup.jsp">Sign Up</a></li>
  </c:if>
  <c:if test = "${isAdmin == '12345'}">
<li><a href="usersTable.jsp">Users Table</a></li>  
  </c:if>
  <c:if test = "${loggedIn == '1'}">
  	<li><a href = "logout.jsp">Logout</a></li>
  </c:if>
</ul>
<hr>
Enter team number: &nbsp; <input type="text" name="team" placeholder="3075"></br> 
Enter username: &nbsp; <input type="text" name="uname" placeholder="Username"><br>
Enter password: &nbsp; <input type="password" name="pass" placeholder="Password"><br>
Enter email: &nbsp; <input type="email" name="mail" placeholder="example@email.com"><br>
Admin Code: &nbsp; <input type="text" name="admin" placeholder="Admin Code">
<hr>
<input type="submit">
<input type="reset" value="Reset">
</form>
<h3>Already registered? <a href="index.jsp">Login Here</a></h3>
</div>
</body>
</html>